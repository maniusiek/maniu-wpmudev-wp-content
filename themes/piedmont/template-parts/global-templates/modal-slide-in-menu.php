<div id="modal-slide-in-menu" class="modal fade modal-slide-in-right modal-full-height nav-styling-underline" tabindex="-1" role="dialog" aria-label="<?php echo esc_attr( 'slide-in menu', 'cpschool' ); ?>" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content has-background">
			<div class="modal-header">
				<button type="button" class="btn-hl-icon" data-dismiss="modal">
					<i aria-hidden="true" class="cps-icon cps-icon-cross-gold"></i>
					<span class="sr-only"><?php _e( 'close menu', 'piedmont' ); ?></span>
				</button>
			</div>
			<div class="modal-body">
				<form class="search-form d-flex" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
					<label class="sr-only" for="search-form-header"><?php _e( 'Search', 'piedmont' ); ?></label>
					<input id="search-form-header" class="form-control form-control-lg" type="search" placeholder="<?php _e( 'Search', 'piedmont' ); ?>" value="<?php echo get_search_query(); ?>" name="s">
					<button type="submit" aria-controls="search-form-header">
						<i aria-hidden="true" class="cps-icon cps-icon-zoom"></i>
						<span class="sr-only"><?php _e( 'Search Site', 'piedmont' ); ?></span>
					</button>
				</form>

				<?php
				wp_nav_menu(
					array(
						'container'            => 'nav',
						'container_class'      => 'nav-container menu-mobile',
						'container_id'         => 'nav-main-extended',
						'container_aria_label' => __( 'extended', 'piedmont' ),
						'menu_id'              => 'menu-main-extended',
						'depth'                => 2,
						'menu_class'           => 'nav flex-column',
						'theme_location'       => 'desktop-extended',
						'walker'               => new CPSchool_WP_Bootstrap_Navwalker(),
					)
				);
				?>
				<div class="nav__title text-uppercase"><?php esc_html_e('Info For…', 'piedmont'); ?></div>
				<div class="nav__wrapper d-flex">
					<?php
					wp_nav_menu(
						array(
							'container'            => 'nav',
							'container_class'      => 'nav-container menu-mobile',
							'container_id'         => 'nav-main-info',
							'container_aria_label' => __( 'Information Menu', 'piedmont' ),
							'menu_id'              => 'menu-main-info',
							'menu_class'           => 'nav flex-column',
							'theme_location'       => 'info',
							'walker'               => new CPSchool_WP_Bootstrap_Navwalker(),
						)
					);
					wp_nav_menu(
						array(
							'theme_location'  => 'desktop',
							'container_class' => 'navbar-nav-container d-flex d-md-none align-items-center',
							'container_id'    => 'navbar-main-nav-mobile',
							'menu_class'      => 'nav flex-column',
							'fallback_cb'     => '',
							'menu_id'         => 'menu-main-mobile',
							'depth'           => 1,
							'walker'          => new CPSchool_WP_Bootstrap_Navwalker( true, false ),
						)
					);
					?>
				</div>
			</div>
		</div>
	</div>
</div><!-- #modal-slide-in-menu -->
