<?php
$hero_style = cpschool_get_hero_style();

if ( $hero_style ) {
	if ( $hero_style != 'breadcrumbs-only' ) {
		$title    = cpschool_get_page_title();
		$subtitle = cpschool_get_page_subtitle();
		if ( is_singular() ) {
			$meta = cpschool_get_post_meta( get_the_ID(), is_singular() );
		} else {
			$meta = false;
		}

		$thumbnail_post_id = cpcs_get_hero_thumbnail_post_id();

		$oval_menu = get_field( 'oval_menu' );

		$hero_classes   = array( 'hero' );
		$hero_classes[] = $hero_style === 'fullwidth' && ! $oval_menu ? 'hero--no-oval' : '';
		$hero_classes[] = 'hero--' . $hero_style;

		if ( $thumbnail_post_id && has_post_thumbnail( $thumbnail_post_id ) ) {
			$hero_classes[] = 'hero--has-bg-image';
		}
		if ( $subtitle ) {
			$hero_classes[] = 'hero--has-subtitle';
		}
		?>
		<header id="hero-main" <?php cpschool_class( 'hero-main', $hero_classes ); ?> aria-label="<?php esc_html_e( 'page title and basic information', 'piedmont' ); ?>">
			<div class="hero__content container" data-aos="fade" data-aos-delay="500" data-aos-duration="1000">
				<div class="hero__title-box">
					<h1 class="hero__title entry-title"><?php echo $title; ?></h1>
					<?php if ( $hero_style === 'fullwidth' && have_rows( 'oval_menu' ) ) : ?>
						<nav class="hero__oval-menu">
							<ul>
								<?php
								while( have_rows( 'oval_menu' ) ) : 
									the_row();
									$title = get_sub_field( 'title' );
									$link  = get_sub_field( 'link' );
									?>
									<li>
										<a href="<?php echo esc_url( $link ); ?>"><?php echo esc_html( $title ); ?></a>
									</li>
								<?php endwhile; ?>
							</ul>
						</nav>
					<?php endif; ?>
				</div>
			</div>

			<?php
			if ( $hero_style != 'regular' ) {
				
				if ( $thumbnail_post_id && has_post_thumbnail( $thumbnail_post_id ) ) {
					?>
					<div class="hero__image-holder" data-aos="fade" data-aos-duration="1000">
						<?php echo get_the_post_thumbnail( $thumbnail_post_id, 'hero' ); ?>
					</div>
					<?php
				}
			}
			?>
		</header>
		<?php
	}
	if ( ( !is_front_page() && in_array( get_post_type(), array( 'post', 'page', 'calendar_event' ) ) ) ) {
		$breadcrumb_class = 'breadcrumb-holder';
		$breadcrumb_class .= $hero_style === 'breadcrumbs-only' ? ' breadcrumb-holder--no-hero' : '';
		$breadcrumb_class .= ' breadcrumb-holder--hero-style-' . $hero_style;
		$breadcrumb_class .= $thumbnail_post_id && has_post_thumbnail( $thumbnail_post_id ) ? ' breadcrumb-holder--hero-has-bg-image' : '';
		?>
		<div class="<?php echo esc_attr( $breadcrumb_class ); ?>">
			<div class="container">
				<div class="breadcrumb-holder__inner">
					<?php cpschool_show_breadcrumb(); ?>
					<?php if ( have_rows( 'menu' ) ) : ?>
						<nav class="breadcrumb-menu">
							<ul>
								<?php
								while( have_rows( 'menu' ) ) :
									the_row();
									$title = get_sub_field( 'title' );
									$link  = get_sub_field( 'link' );
									?>
									<li>
										<a href="<?php echo esc_url( $link ); ?>"><?php echo esc_html( $title ); ?> —></a>
									</li>
									<?php
								endwhile;
								?>
							</ul>
						</nav>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php
	}
}