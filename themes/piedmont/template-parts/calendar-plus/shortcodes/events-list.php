<div class="events-list">
<?php
wp_dequeue_style( 'calendarp-events-by-cat' );
foreach ( $event_groups as $events_by_date ) {
	foreach ( $events_by_date as $date => $events ) {
		$month_name = mysql2date( 'M', $date, true );
		$day        = mysql2date( 'd', $date, true );
		?>
		<ul class="post-list post-list--has-date">
			<?php
			foreach ( $events as $event ) :
				?>
				<li class="post-list__post">
					<div class="post-list__date">
						<span><?php echo $month_name; ?></span>
						<?php echo $day; ?>
					</div>
					<div class="post-list__details">
						<a href="<?php esc_url( get_permalink($event->ID) ); ?>" class="post-list__title">
							<?php echo get_the_title( $event->ID  ); ?>
						</a>
						<?php
						$event_date = calendarp_get_human_read_dates( $event->ID, 'array' );
						$splited_time = explode( ' - ', $event_date['time'] );
						$start_time = ! empty( $splited_time[0] ) ? $splited_time[0] : '';
						$location = calendarp_the_event_location( $event->ID );
						if($event_date['time'] || $location) {
							?>
							<div class="post-list__meta">
								<?php
								if( ! empty( $start_time ) ) {
									echo $start_time;
									if($location) {
										echo ', ';
									}
								}
								if($location) {
									echo $location->get_full_address();
								}
								?>
								<span class="post-list__meta-details" aria-hidden="true">/ <a class="post-list__meta-details-link" href="<?php echo esc_url( get_permalink($event->ID) ); ?>"><?php esc_html_e( 'Details', 'piedmont' ); ?></a></span>
							</div>
							<?php 
						}
						?>
					</div>
                </li>
				<?php
			endforeach;
			?>
		</ul>
		<?php
	}
}
?>
</div>
