<?php
if( have_posts() ) :
    while ( have_posts() ) :
        the_post();
        
        get_template_part( 'template-parts/calendar-plus/events-list-event' );
    endwhile;
else :
    ?>
    <div class="h6">No Events Found.</div>
    <?php
endif;
?>