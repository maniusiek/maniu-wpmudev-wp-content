<?php
$date       = calendarp_get_human_read_dates( get_the_ID(), 'array' );
$location   = calendarp_the_event_location( get_the_ID() );
$month_name = date( 'M', strtotime( $date['date'] ) );
$day        = date( 'd', strtotime( $date['date'] ) );
?>

<a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" class="events-list-item">
    <div class="events-list-item-date">
        <span class="events-list-item-date-month"><?php echo $month_name; ?></span>
        <span class="events-list-item-date-day"><?php echo $day; ?></span>
    </div>
    <div class="events-list-item-details">
        <span class="events-list-item-title">
            <?php echo get_the_title( get_the_ID() ); ?>
        </span>
        <?php if( is_post_type_archive( 'calendar_event' ) ) { ?> 
            <div class="events-list-item-excerpt">
                <?php echo get_the_excerpt( get_the_ID() ); ?>
            </div>
        <?php } ?> 
        <div class="events-list-item-meta">
            <?php
            if ( $date['time'] ) {
                echo '<i class="cps-icon cps-icon-clock" aria-hidden="true"></i>';
                echo $date['time'];
                if ( $location ) {
                    echo ' / ';
                }
            }
            if ( $location ) {
                echo '<i class="cps-icon cps-icon-map-marker" aria-hidden="true"></i>';
                echo $location->get_full_address();
            }
            ?>
        </div>
    </div>
</a>