<?php
$class_names = 'wp-block-featured-event featured-event';

$categories = get_field( 'category' );

$current_time = current_time( 'timestamp' );

$args = array(
    'category'        => $categories,
    'events_per_page' => 3,
);

$events_by_date = calendarp_get_events_in_date_range( $current_time, false, $args );
?>
<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>">
<?php
foreach ( $events_by_date as $date => $events ) :
    $month_name = mysql2date( 'M', $date, true );
    $day        = mysql2date( 'd', $date, true );

    
    foreach ( $events as $event ) : 
        $event_tags  = get_the_terms( $event->ID, 'calendar_event_tag' );
        $location    = function_exists( 'calendarp_the_event_location' ) ? calendarp_the_event_location( $event->ID ) : false;
        $from_month  = mysql2date( 'F', $event->dates_data['from_date'] );
        $from_day    = mysql2date( 'j', $event->dates_data['from_date'] );
        $until_month = mysql2date( 'F', $event->dates_data['until_date'] );
        $until_day   = mysql2date( 'j', $event->dates_data['until_date'] );
        ?>
        <div class="featured-event__event">
            <div class="featured-event__thumb">
                <a href="<?php echo esc_url( get_permalink( $event->ID ) ); ?>"><?php echo get_the_post_thumbnail( $event->ID, 'featured_events' ); ?></a>
                <?php if ( ! is_wp_error( $event_tags ) && ! empty( $event_tags ) ) : ?>
                    <div class="featured-event__tag"><?php echo $event_tags[0]->name; ?></div>
                <?php endif; ?>
            </div>
            <a href="<?php echo esc_url( get_permalink( $event->ID ) ); ?>" class="featured-event__title">
                <?php echo get_the_title( $event->ID ); ?>
            </a>
            <div class="featured-event__date">
            <?php echo "$from_month $from_day"; ?><?php echo ( $from_month !== $until_month ? '-' . $until_month . ' ' : '' ); ?><?php echo ( $from_month === $until_month && $until_day === $from_day ? '' : ( $from_month === $until_month && $until_day !== $from_day ? '-' : '' ) . $until_day ); ?>
            </div>
            <?php if ( $location ) : ?>
                <div class="featured-event__location"><?php echo $location->get_full_address(); ?></div>
            <?php endif; ?>
            <div class="featured-event__more">
                <a class="featured-event__more-link right-arrow" href="<?php echo esc_url( get_permalink( $event->ID ) ); ?>"><?php esc_html_e( 'Event Details', 'piedmont' ); ?></a>
            </div>
        </div>
    <?php endforeach;
endforeach;
?>
</div>
