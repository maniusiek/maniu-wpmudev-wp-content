<?php
if ( have_rows( 'slides' ) ) :
	$class_names = 'wp-block-hero-slider hero-slider';

	$title        = get_field( 'title' );
	$button_label = get_field( 'button_label' );
	$button_url   = get_field( 'button_url' );
	?>
	<section id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>" aria-label="<?php _e( 'Hero Slider' ); ?>">
		<div class="hero-slider__slider">
			<?php
				while ( have_rows( 'slides' ) ) :
					the_row();

					$image        = get_sub_field( 'image' );
					$row_index    = get_row_index();
					?>
					<div class="hero slick-slide">
						<?php
							if ( isset( $image['id'] ) ) {
								?>
								<div class="hero__image-holder">
									<?php echo wp_get_attachment_image( $image['id'], 'hero' ); ?>
								</div>
								<?php
							}
						?>
					</div>
					<?php
				endwhile;
			?>
		</div>
		<div class="hero-slider__content-wrap hero">
			<div class="hero__content container">
				<div class="hero__content-inner <?php echo ( ! have_rows( 'menu_items' ) ? 'hero__content-inner--no-nav' : '' ) ?>">
					<?php if ( ! empty( $title ) ) : ?>
						<div class="hero__title-wrap">
							<h2 id="<?php echo esc_attr( $block['id'] ); ?>__title-<?php echo esc_attr( $row_index ); ?>" class="hero__title is-green-alternate-border-bg"><?php echo $title; ?></h2>
						</div>
					<?php endif; ?>
					<?php if ( ! empty( $button_label ) ) : ?>
						<a href="<?php echo esc_url( $button_url ); ?>" class="btn btn--dark-bg"
							<?php
							if ( ! empty( $title ) ) {
								echo ' aria-laballedby="' . esc_attr( $block['id'] ) . '__title-' . esc_attr( $row_index ) .'"';}
							?>
						>
							<?php echo esc_html( $button_label ); ?>
						</a>
					<?php endif;
					if ( have_rows( 'menu_items' ) ) : ?>
						<div class="hero__nav">
							<div class="hero__nav-title"><?php _e( 'Choose your path…' ) ?></div>
							<ul class="hero__nav-items">
								<?php
								while ( have_rows( 'menu_items' ) ) :
									the_row();
									$title       = get_sub_field( 'title' );
									$description = get_sub_field( 'description' );
									$url         = get_sub_field( 'url' );
									?>
									<li>
										<a href="<?php echo $url; ?>"><?php echo $title; ?></a>
										<?php if ( ! empty( $description ) ) : ?>
											<span class="hero__nav-item-desc"><?php echo $description; ?></span>
										<?php endif; ?>
									</li>
									<?php
								endwhile;
								?>
							</ul>
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<?php
endif;
