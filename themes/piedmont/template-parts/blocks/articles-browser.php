<?php
$class_name = 'wp-block-articles-browser articles-browser posts-browser';

if ( ! empty( $block['className'] ) ) {
	$class_name .= ' ' . $block['className'];
}

$excluded_categories = get_field( 'excluded_categories' );

$args  = array(
	'posts_per_page'   => 6,
	'category__not_in' => $excluded_categories,
	'cat'              => '',
	'paged'            => 1,
);

$posts = get_posts( $args );

if ( $posts ) :
    $categories = get_terms(
		'category',
		array(
			'hide_empty' => true,
			'exclude'    => $excluded_categories,
		)
    );
    ?>
    <div class="<?php echo esc_attr( $class_name ); ?>" data-query-args="<?php echo esc_attr( json_encode( $args ) ); ?>">
        <div class="posts-browser__nav">
			<h3 class="posts-browser__heading is-style-short-background"><?php _e( 'Recent News' ); ?></h3>
			<form class="posts-browser__cat-picker">
				<div class="frm-dropdown dropdown" data-controls="articles-browser-<?php echo esc_attr( $block['id'] ); ?>">
					<span class="sr-only"><?php _e( 'Selecting category will refresh news below instantly' ); ?></span>
					<button type="button"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="default">
							<?php _e( 'Categories' ); ?>
						</span>
						<span class="current"></span>
					</button>
					<div class="dropdown-menu">
						<div class="dropdown-item">
							<button type="button" data-query-arg="cat" data-value="0">
								<?php echo _e( 'All' ); ?>
							</button>
						</div>
						<?php foreach ( $categories as $category ) { ?>
							<div class="dropdown-item">
								<button type="button" data-query-arg="cat" data-value="<?php echo esc_attr( $category->term_id ); ?>">
									<?php echo $category->name; ?>
								</button>
							</div>
						<?php } ?>
					</div>
				</div>
			</form>
		</div>
		<ul class="articles-browser__post-list post-list post-list--browser" id="articles-browser-<?php echo esc_attr( $block['id'] ); ?>">
            <?php
            echo '<li class="sr-only" tabindex="-1">' . sprintf( __( '%d news loaded' ), count($posts) ) . '</li>';
			foreach ( $posts as $post_object ) {
				include( get_stylesheet_directory() . '/inc/child/posts-browser/post.php' );
			}
			?>
		</ul>
		<div class="articles-browser__more text-center">
			<div class="wp-block-button is-style-with-arrow">
				<button type="button" class="wp-block-button__link" data-action="load-more">
					<?php _e( 'More News' ); ?>
				</button>
			</div>
		</div>
    </div>
    <?php
endif;
