<?php
$events_cat  = get_field( 'event_category' );
$count       = get_field( 'count' );
$class_names = 'wp-block-important-dates important-dates';

/**
 * Build args for event list query.
 */
$args = array(
	'category'        => $events_cat,
	'events_per_page' => $count,
	'grouped_by_day'  => false
);

$current_time = current_time( 'timestamp' );
$sticky_ids   = get_option( 'sticky_posts' );

/**
 * Get events list
 */
if ( empty( $sticky_ids ) ) {

	$events = array(
		calendarp_get_events_in_date_range( $current_time, false, $args ),
	);

} else {

	$events = array(
		calendarp_get_events_in_date_range( $current_time, false, $args + array( 'include_ids' => $sticky_ids ) ),
		calendarp_get_events_in_date_range( $current_time, false, $args + array( 'exclude_ids' => $sticky_ids ) ),
	);
}

$events    = $events[0];
$url       = '';
$url_label = '';

?>
<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>"
     aria-label="<?php _e( 'Important Dates', 'piedmont' ); ?>">
	<?php
	if ( ! empty( $events ) ) :
		?>
		<ul class="post-list post-list--has-date">
			<?php
			foreach ( $events as $event ) {
				$month_name = mysql2date( 'M', $event->from_date, true );
				$day        = mysql2date( 'd', $event->from_date, true );

				if ( function_exists( 'get_field' ) ) {
					$url       = get_field( 'url', $event->event_id );
					$url_label = get_field( 'url_label', $event->event_id );
				}
				?>
				<li class="post-list__post">
					<div class="post-list__date">
						<?php echo $month_name; ?>
						<?php echo $day; ?>
					</div>
					<div class="post-list__details">
						<a href="<?php echo esc_url( get_permalink( $event->event_id ) ); ?>"
						   class="post-list__title">
							<?php echo get_the_title( $event->event_id ); ?>
						</a>
						<?php
						if ( ! empty( $url ) && ! empty( $url_label ) ) :
							?>
							<a href="<?php echo esc_url( $url ); ?>"
							   class="post-list__link"><?php echo $url_label; ?></a>
						<?php
						endif;
						?>
					</div>
				</li>
				<?php
			}
			?>
		</ul>
	<?php
	endif;
	?>
</div>
