
<?php
$class_name = 'wp-block-separator';
if ( ! empty( $block['className'] ) ) {
    $class_name .= ' ' . $block['className'];
}
?>
<div class="<?php echo esc_attr( $class_name ); ?>">
    <?php cpcs_social_links( 'block' ); ?>
</div>