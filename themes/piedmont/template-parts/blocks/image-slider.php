<?php

if ( have_rows( 'slides' ) ) :
	$class_names = 'wp-block-image-slider image-slider';
	$slider_title = get_field( 'slider_title' );
	?>
	<section id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>" aria-label="<?php _e( 'Image Slider' ); ?>">
		<h2 class="image-slider__title h"><?php echo $slider_title; ?></h2>
		<div class="image-slider__slides container">
			<?php
				while ( have_rows( 'slides' ) ) :
					the_row();

					$image        = get_sub_field( 'image' );
					$title        = get_sub_field( 'title' );
					$description  = get_sub_field( 'description' );
					$button_label = get_sub_field( 'button_label' );
					$button_url   = get_sub_field( 'button_url' );
					?>
					<div class="image-slider__slide slick-slide">
						<div class="image-slider__slide-inner">
							
							<?php
								if ( isset( $image['id'] ) ) {
									?>
									<div class="image-slider__slide-image">
										<?php echo wp_get_attachment_image( $image['id'], 'image_slider' ); ?>
									</div>
									<?php
								}
							?>
							<div class="image-slider__content">
								<h3 class="image-slider__content-title h">
									<span><?php echo $title; ?></span>
								</h3>
								<div class="image-slider__content-block">
									<div class="image-slider__content-description">
										<?php echo $description; ?>
									</div>
									<?php if ( ! empty( $button_label ) ) : ?>
										<a class="image-slider__link" href="<?php echo esc_url( $button_url ); ?>"><?php echo esc_html( $button_label ); ?> —></a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
					<?php

				endwhile;
			?>
		</div>
	</section>
	<?php
endif;