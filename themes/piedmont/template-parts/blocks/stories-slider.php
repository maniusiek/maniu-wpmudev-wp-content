<?php

if ( have_rows( 'stories' ) ) :
	$class_names = 'wp-block__story-slider story-slider';
	?>
	<section id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>"
	         aria-label="<?php _e( 'Stories of student success' ); ?>">
		<h3 class="is-style-background--solid"><?php echo esc_html( get_field( 'title' ) ); ?></h3>
		<div class="story-slider__slides container">
			<?php
			while ( have_rows( 'stories' ) ) :
				the_row();
				$post = get_sub_field( 'post' );
				if ( ! empty( $post ) ) :
					$title       = $post->post_title;
					$description = get_the_excerpt( $post );
					$image_id    = get_post_thumbnail_id( $post );
					$url         = get_permalink( $post );
				elseif ( $content = get_sub_field( 'slide_content' ) ):
					$title       = $content['title'];
					$description = $content['description'];
					$image_id    = $content['image'];
					$url         = $content['url'];
				endif;
				?>
				<div class="story-slider__slide">
					<div class="story-slider__slide-inner">

						<?php
						if ( isset( $image_id ) ) {
							?>
							<div class="story-slider__slide-image">
								<?php echo wp_get_attachment_image( $image_id, 'story_slider' ); ?>
							</div>
							<?php
						}
						?>
						<div class="story-slider__content">
							<h5 class="story-slider__content-title"><?php echo $title; ?></h5>
							<div class="story-slider__content-block">
								<div class="story-slider__content-description">
									<?php echo $description; ?>
								</div>
								<a class="story-slider__link right-arrow"
								   href="<?php echo esc_url( $url ); ?>"><?php echo esc_html__( 'Keep reading', 'piedmont' ); ?></a>
							</div>
						</div>
					</div>
				</div>
			<?php
			endwhile;
			?>
		</div>
	</section>
<?php
endif;