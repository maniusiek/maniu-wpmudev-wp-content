<?php
$class_names = 'wp-block-image-gallery image-gallery';

$dots_position = get_field( 'dots_position' );
$show_arrow    = get_field( 'show_arrow' );
$style         = get_field( 'style' );
$content_position = get_field( 'content_position' );
$title         = get_field( 'title' );
$description   = get_field( 'description' );
$half_of_viewport   = get_field( 'half_of_viewport' );


if ( ! $style ) {
	$style = 'default';
}

if ( ! $content_position ) {
	$content_position = 'right';
}

$class_names .= ' image-gallery--style-' . $style;
$class_names .= ' image-gallery--content-pos-' . $content_position;

if ( ! empty( $dots_position ) ) {
	$class_names .= ' image-gallery--dots-pos-' . $dots_position;
}

if ( $half_of_viewport ) {
	$class_names .= ' image-gallery--is-half-of-viewport';
}

if ( ! empty( $show_arrow ) ) {
	$class_names .= ' image-gallery--show-arrow-' . $show_arrow;
}

?>
<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>" aria-label="<?php _e( 'Image Gallery' ); ?>">
	<?php if ( have_rows( 'images' ) ) : ?>
		<div class="image-gallery__slider">
			<?php
			while ( have_rows( 'images' ) ) :
				the_row();
				$image = get_sub_field( 'image' );

				if ( $style === 'dynamic' ) {
					$title        = get_sub_field( 'title' );
					$description  = get_sub_field( 'description' );
					$button_label = get_sub_field( 'button_label' );
					$button_link  = get_sub_field( 'button_link' );
				}

				if ( $image ) :
					?>
					<div class="image-gallery__image slick-slide">
						<?php echo wp_get_attachment_image( $image, 'image_gallery' ); ?>

						<?php if ( $style === 'dynamic' ) : ?>
							<div class="image-gallery__content">
								<div class="image-gallery__content-inner">
									<div class="image-gallery__title"><?php echo $title; ?></div>
									<div class="image-gallery__description"><?php echo $description; ?></div>
									<?php if ( ! empty( $button_label ) ) : ?>
										<a class="image-gallery__link right-arrow" href="<?php echo esc_url( $button_link ); ?>"><?php echo esc_html( $button_label ); ?></a>
									<?php endif; ?>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php
				endif;
			endwhile;
			?>
		</div>
		<?php if ( $style === 'static' ) : ?>
			<div class="image-gallery__content">
				<div class="image-gallery__content-inner">
					<div class="image-gallery__title"><?php echo $title; ?></div>
					<div class="image-gallery__description"><?php echo $description; ?></div>
					<?php if ( have_rows( 'buttons' ) ) : ?>
						<ul class="image-gallery__links">
							<?php while( have_rows( 'buttons' ) ) :
								the_row();
								$button_label = get_sub_field( 'button_label' );
								$button_link  = get_sub_field( 'button_link' );
								?>
								<li>
									<a class="image-gallery__link right-arrow" href="<?php echo esc_url( $button_link ); ?>"><?php echo esc_html( $button_label ); ?></a>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</div>