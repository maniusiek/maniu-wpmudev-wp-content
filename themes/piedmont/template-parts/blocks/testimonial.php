<?php
$class_names = 'wp-block-testimonial testimonial';

if ( ! empty( $block['align'] ) ) {
	$class_names .= ' testimonial--align-' . $block['align'];
}

$styles    = get_field('styles');
$content   = get_field('content');
$full_name = get_field('full_name');
$degree    = get_field('degree');
$thumbnail = get_field('thumbnail');

if ( empty( $styles ) ) {
	$styles = 'default';
}

$class_names .= ' testimonial--style-' . $styles;

if ( 'secondary' ===  $styles && ! empty( $thumbnail ) ) {
	$class_names .= ' testimonial--has-thumbnail';
}

$name_degree = array();

if ( ! empty( $full_name ) ) {
	$name_degree[] = '<strong>' . $full_name . '</strong>';
}

if ( ! empty( $degree ) ) {
	$name_degree[] = $degree;
}

$sep = 'centered' ===  $styles ? ' / ' : ', ';

?>
<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>">
	<div class="testimonial__inner">
		<div class="testimonial__content">
			<div class="testimonial__content-inner"><?php echo $content; ?></div>
		</div>
		<div class="testimonial__name">
			<div class="testimonial__name-inner">
				<?php echo implode( $sep, $name_degree ); ?>
			</div>
		</div>
	</div>
	<?php if ( 'secondary' ===  $styles && ! empty( $thumbnail ) ) : ?>
		<div class="testimonial__thumbnail"><?php echo wp_get_attachment_image( $thumbnail, 'testimonial_thumbnail' ); ?></div>
	<?php endif; ?>
</div>