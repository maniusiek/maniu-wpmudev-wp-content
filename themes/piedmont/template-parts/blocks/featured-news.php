<?php
$class_names = 'wp-block-featured-news featured-news post-list';

$categories     = get_field( 'category' );
$number_of_news = get_field( 'number_of_news' );
$more_link      = get_field( 'more_link' );

$args = array(
    'post_type'      => 'post',
    'posts_per_page' => $number_of_news,
    'category__in'   => $categories,
);

$news = new WP_Query( $args );

if ( $news->have_posts() ) :
    ?>
    <div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>">
    <?php
        while( $news->have_posts() ) :
            $news->the_post();
            ?>
            <div class="post-list__post">
                <?php the_title( '<a class="post-list__title" href="' . get_the_permalink() . '">', '</a>' ); ?>
                <div class="post-list__content"><?php echo get_the_excerpt(); ?></div>
                <a class="post-list__button right-arrow" href="<?php the_permalink(); ?>"><?php _e( 'Keep Reading' ); ?></a>
            </div>
            <?php
        endwhile;
        wp_reset_postdata();
    ?>
        <div class="wp-block-button is-style-with-arrow">
            <a class="wp-block-button__link" href="<?php echo esc_url( $more_link ); ?>"><?php _e( 'More News' ); ?></a>
        </div>
    </div>
    <?php
endif;
