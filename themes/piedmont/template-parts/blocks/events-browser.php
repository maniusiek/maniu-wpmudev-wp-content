<?php
$class_name = 'wp-block-events-browser events-browser posts-browser';
if ( ! empty( $block['className'] ) ) {
	$class_name .= ' ' . $block['className'];
}

$excluded_categories = get_field( 'excluded_categories' );

$args = $args_attr = array(
	'post_type'        => 'calendar_event',
	'posts_per_page'   => 2,
	'paged'            => 1,
	'from'             => date( 'Y-m-d' ),
	'suppress_filters' => false,
	'order' => 'ASC'
);
if ( $excluded_categories ) {
	$args_attr['tax__not_in'] = array( 'calendar_event_category' => $excluded_categories );
	$args['tax_query']        = array(
		array(
			'taxonomy' => $tax_name,
			'field'    => 'term_id',
			'terms'    => $tax_value,
			'operator' => 'NOT IN',
		),
	);
}


$posts = get_posts( $args );
if ( $posts ) {
	$categories = get_terms(
		'calendar_event_category',
		array(
			'hide_empty' => true,
			'exclude'    => $excluded_categories,
		)
	);
	?>
	<div class="<?php echo esc_attr( $class_name ); ?>" data-query-args="<?php echo esc_attr( json_encode( $args ) ); ?>">
		<div class="content-sidebar">
			<div class="content-sidebar__sidebar">
				<?php the_widget( 'Calendar_Plus_Calendar_Widget' ); ?>
			</div>
			<div class="content-sidebar__content">
				<div class="posts-browser__nav">
					<div class="events-browser__title">
						<h3 class="is-style-short-background"><?php _e( 'Coming Soon', 'piedmont' ); ?></h3>
					</div>
					<form class="posts-browser__cat-picker">
						<div class="frm-dropdown dropdown" data-controls="events-browser-<?php echo esc_attr( $block['id'] ); ?>">
							<span class="sr-only"><?php _e( 'Selecting category will refresh events on page instantly', 'piedmont' ); ?></span>
							<button type="button"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="default">
									<?php _e( 'Categories', 'piedmont' ); ?>
								</span>
								<span class="current"></span>
							</button>
							<div class="dropdown-menu">
								<div class="dropdown-item">
									<button type="button" data-query-arg="tax" data-value="<?php echo esc_attr( json_encode( array( 'calendar_event_category' => 0 ) ) ); ?>">
										<?php echo _e( 'All', 'piedmont' ); ?>
									</button>
								</div>
								<?php
								foreach ( $categories as $category ) {
									$tax = array( 'calendar_event_category' => $category->term_id );
									?>
									<div class="dropdown-item">
										<button type="button" data-query-arg="tax" data-value="<?php echo esc_attr( json_encode( $tax ) ); ?>"">
											<?php echo $category->name; ?>
										</button>
									</div>
								<?php } ?>
							</div>
						</div>
					</form>
				</div>
				<ul class="events-browser__post-list post-list post-list--has-date" id="events-browser-<?php echo esc_attr( $block['id'] ); ?>">
					<?php
					foreach ( $posts as $post_object ) {
						include( get_stylesheet_directory() . '/inc/child/posts-browser/calendar_event.php' );
					}
					?>
				</ul>
				<div class="events-browser__more">
					<button type="button" class="btn" data-action="load-more">
						<?php _e( 'Load more events', 'piedmont' ); ?>
						<i class="cps-icon cps-icon--arrow-right" aria-hidden="true"></i>
					</button>
				</div>
			</div>
		</div>
	</div>

	<?php
}
