<?php
if ( have_rows( 'stats' ) ) :
    $class_names = 'wp-block-stats-slider stats-slider';

    $over_content = get_field('over_content');
    if ( $over_content ) {
        $class_names .= ' stats-slider--over-content';
    }
    ?>
    <div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>" aria-label="<?php _e( 'Stats Slider' ); ?>">
        <?php
        while( have_rows( 'stats' ) ) :
            the_row();

            $number       = get_sub_field( 'number' );
            $description  = get_sub_field( 'description' );
            ?>
            <div class="stats-slider__slide slick-slide">
                <div class="stats-slider__number"><?php echo $number; ?></div>
                <div class="stats-slider__description"><?php echo $description; ?></div>
            </div>
            <?php
        endwhile;
        ?>
    </div>
    <?php
endif;