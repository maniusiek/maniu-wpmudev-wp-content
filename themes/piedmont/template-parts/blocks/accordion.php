<?php
if ( have_rows( 'items' ) ) :
	$class_names = 'wp-block-accordion accordion';
	?>
	<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>">
		<?php
		while( have_rows( 'items' ) ):
			the_row();
			$index = get_row_index();

			$title   = get_sub_field( 'title' );
			$content = get_sub_field( 'content' );

			$content_id = $block['id'] . '-accordion-content-' . $index;
			$heading_id = $block['id'] . '-accordion-heading-' . $index;
			?>
			<div class="accordion__item" >
				<button class="accordion__item-heading collapsed" id="<?php echo esc_attr( $heading_id ); ?>" data-toggle="collapse" data-target="#<?php echo esc_attr( $content_id ); ?>" aria-expanded="false" aria-controls="<?php echo esc_attr( $content_id ); ?>"><?php echo $title; ?></button>
				<div class="accordion__item-content-wrapper collapse" id="<?php echo esc_attr( $content_id ); ?>" aria-labelledby="<?php echo esc_attr( $heading_id ); ?>"> 
					<div class="accordion__item-content">
						<?php echo $content; ?>
					</div>
				</div>
			</div>
			<?php
		endwhile;
		?>
	</div>
	<?php
endif;