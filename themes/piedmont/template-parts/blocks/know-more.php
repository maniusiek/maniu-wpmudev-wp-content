<?php
$class_names = 'wp-block-know-more know-more';

if ( have_rows( 'add_link' ) ):
	?>
	<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_names ); ?>">
		<?php
		while ( have_rows( 'add_link' ) ):
			the_row();

			$label = get_sub_field( 'label' );
			$url   = get_sub_field( 'url' );
			?>
			<a href="<?php echo esc_url_raw( $url ); ?>" class="right-arrow"><?php echo esc_html( $label ); ?></a>
		<?php
		endwhile;
		?>
	</div>
<?php
endif;
