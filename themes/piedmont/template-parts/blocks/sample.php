<?php
$class_name = 'wp-block-slug slug';
if ( ! empty( $block['className'] ) ) {
	$class_name .= ' ' . $block['className'];
}
if ( ! empty( $block['align'] ) ) {
	$class_name .= ' align' . $block['align'];
}

$style = get_field( 'style' );
if ( $style ) {
	$class_name .= ' slug--' . $style;
}

?>
<div id="<?php echo esc_attr( $block['id'] ); ?>" class="<?php echo esc_attr( $class_name ); ?>">
		<?php
		while ( have_rows( 'repeater_field' ) ) :
			the_row();

			$sub_field = get_sub_field( 'sub_field' );
		endwhile;
		?>
	</div>
</div>