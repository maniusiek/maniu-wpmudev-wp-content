<?php
/**
 * Functions and definitions
 *
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

// Loads all the necessary files
require_once 'inc/child/template-tags.php';
require_once 'inc/child/acf.php';
require_once 'inc/child/blocks/content-sidebar/content-sidebar.php';
require_once 'inc/setup-theme.php';
require_once 'inc/child/posts-browser.php';
require_once 'inc/child/events.php';

/**
 * Load only selected functions from parent theme
 */
add_filter(
	'cpschool_includes',
	function( $cpschool_includes ) {
		$allowed = array(
			'calendar-plus',
			'wp-bootstrap-navwalker',
			'template-tags',
			'hooks-wp',
			'breadcrumbs',
		);

		foreach ( $cpschool_includes as $includes_key => $includes_file ) {
			if ( ! in_array( $includes_key, $allowed ) ) {
				unset( $cpschool_includes[ $includes_key ] );
			}
		}
		return $cpschool_includes;
	}
);

/**
 * Load theme's JavaScript and CSS sources.
 */
add_action(
	'wp_enqueue_scripts',
	function() {
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_stylesheet_directory() . '/css/theme.min.css' );
		wp_enqueue_style( 'cpsc-styles', get_stylesheet_directory_uri() . '/css/theme.min.css', array(), $css_version );

		$js_version = $theme_version . '.' . filemtime( get_stylesheet_directory() . '/js/theme.min.js' );
		wp_enqueue_script( 'cpsc-scripts', get_stylesheet_directory_uri() . '/js/theme.min.js', array( 'jquery' ), $js_version, true );
		wp_localize_script( 'cpsc-scripts', 'cpsc', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
	}
);

/**
 * Loads Editor CSS.
 */
add_action(
	'enqueue_block_editor_assets',
	function() {
		$css_version = filemtime( get_stylesheet_directory() . '/css/block-editor.min.css' );
		wp_enqueue_style( 'cpschool-gutenberg', get_theme_file_uri( 'css/block-editor.min.css' ), false, $css_version );
	}
);

add_action(
	'enqueue_block_editor_assets',
	function() {
		wp_enqueue_script(
			'cpsc-blocks',
			get_stylesheet_directory_uri() . '/js/blocks.js',
			array( 'wp-blocks', 'wp-dom' ),
			filemtime( get_stylesheet_directory() . '/js/blocks.js' ),
			true
		);
	}
);
