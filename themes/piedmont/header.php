<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>
	<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<?php wp_head(); ?><link rel="preconnect" href="https://fonts.gstatic.com">
		<link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Raleway:ital,wght@0,400;0,500;0,700;0,800;0,900;1,400;1,500;1,700;1,800;1,900&display=swap" rel="stylesheet">
	</head>

<body <?php body_class(); ?>>
<?php do_action( 'wp_body_open' ); ?>

<div <?php cpschool_class( 'site-page-wrapper', 'site' ); ?> id="page">
	<a class="skip-link sr-only sr-only-focusable" href="#content"><?php esc_html_e( 'Skip to content', 'cpschool' ); ?></a>

	<?php get_template_part( 'template-parts/global-templates/bar', 'alert' ); ?>

	<?php get_template_part( 'template-parts/global-templates/navbar', 'secondary' ); ?>
	<div id="wrapper-navbar-main" <?php cpschool_class( 'navbar-main-wrapper', 'wrapper-navbar' ); ?> itemscope itemtype="http://schema.org/WebSite">
		<nav id="navbar-main" <?php cpschool_class( 'navbar-main', 'navbar navbar-expand-md nav-styling-underline has-background has-header-main-bg-color-background-color' ); ?> aria-label="<?php esc_html_e( 'main', 'cpschool' ); ?>">
			<div class="navbar-container container-fluid d-flex justify-content-between align-items-center">
				<?php do_action( 'cpschool_navbar_main_container_start' ); ?>

				<div class="navbar-brand-holder">
					<a href="<?php echo get_site_url(); ?>" class="navbar-brand custom-logo-link" rel="home">
						<img width="176" height="36" src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.svg" class="img-fluid" alt="Piedmont University">
					</a>
				</div>

				<div class="navbar-navs-container d-flex justify-content-between">
					<?php
					wp_nav_menu(
						array(
							'theme_location'  => 'desktop',
							'container_class' => 'navbar-nav-container d-none d-md-flex align-items-center',
							'container_id'    => 'navbar-main-nav-desktop',
							'menu_class'      => 'nav navbar-nav',
							'fallback_cb'     => '',
							'menu_id'         => 'menu-main-desktop',
							'depth'           => 1,
							'walker'          => new CPSchool_WP_Bootstrap_Navwalker( true, false ),
						)
					);
					?>

					<div id="navbar-main-nav-buttons">
						<ul class="nav navbar-nav navbar-button-nav-right">
							<li id="navbar-main-btn-slide-in-menu">
								<button class="btn-hl-icon" data-toggle="modal" data-target="#modal-slide-in-menu">
									<i aria-hidden="true" class="cps-icon cps-icon-menu"></i>
									<span class="menu__label" aria-hidden="true" class="d-sm-none d-md-inline"><?php _e( 'Menu' ); ?></span>
									<span class="sr-only"><?php _e( 'Toggle Menu', 'cpschool' ); ?></span>
								</button>
							</li>
						</ul>
					</div>
				</div>

				<?php do_action( 'cpschool_navbar_main_container_end' ); ?>

			</div><!-- #navbar-container -->
		</nav>
	</div><!-- #wrapper-navbar end -->

<?php
get_template_part( 'template-parts/global-templates/hero', 'pagetitle' );
