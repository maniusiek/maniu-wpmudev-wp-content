(function ($) {
	$(document).on('ready', function () {
		// Hero slider.
		$( '.hero-slider .hero-slider__slider' ).slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			arrows: false,
			infinite: true,
			autoplay: true,
			// pauseOnHover: true,
		} );
		// Image Gallery
		$( '.image-gallery .image-gallery__slider' ).each( function() {
			var $this = $( this ),
				slides = $this.find( '.image-gallery__image' );
			if ( slides.length > 1 ) {
				$this.slick( {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true,
					arrows: true,
					infinite: true,
				} );
			}
		} );
		
		// Image Slider
		$( '.image-slider .image-slider__slides' ).slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: true,
			infinite: true,
		} );
		// Stories Slider
		$( '.story-slider .story-slider__slides' ).slick( {
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			arrows: false,
			infinite: true,
		} );

		// Stats Slider
		$( '.stats-slider' ).each( function() {
			var $this = $( this ),
				slides = $this.find( '.stats-slider__slide' );
			console.log(slides)
			if ( slides.length > 5 ) {
				$( $this ).slick( {
					slidesToShow: 5,
					slidesToScroll: 5,
					dots: true,
					arrows: true,
					infinite: false,
					responsive: [
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3,
								// vertical: true,
							}
						},
						{
							breakpoint: 782,
							settings: {
								vertical: true,
							}
						}
					]
				} );
			}
		} );

        // Post loading engine.
        $('.posts-browser').on('click', '[data-action="load-more"]', function (event) {
            event.preventDefault();

            var $element = $(this);
            var $browser = $element.parents('.posts-browser');
            var $postList = $browser.find('.post-list');

            $browser.find('.post-list');
            $(document.body).css({ 'cursor': 'wait' });

            var args = $browser.data('query-args');
            args['paged']++;

            $(document.body).css({ 'cursor': 'wait' });
            $browser.addClass('posts-browser--loading');

            var data = {
                action: 'cpsc_load_posts',
                args: args,
            };
            $.post(cpsc.ajax_url, data, function (response) {
                $postList.append(response.html);
                $postList.find('sr-only').last().trigger('focus');

                if (response) {
                    $(document.body).css({ 'cursor': 'default' });
                    $browser.removeClass('posts-browser--loading');

                    if (args['posts_per_page'] > response.found) {
                        $element.hide();
                    }
                }
            });
        });

        // Post quering engine.
        $('.posts-browser').on('click', '[data-query-arg]', function () {
            var val = $(this).data('value');
            var arg = $(this).data('query-arg');

            var $element = $(this);
            var $browser = $element.parents('.posts-browser');
            var $postList = $browser.find('.post-list');

            var args = $browser.data('query-args');

            if( args[arg] !== val ) {
                args[arg] = val;
                args['paged'] = 1;

                $browser.data('query-args', args);

                $(document.body).css({ 'cursor': 'wait' });
                $browser.addClass('posts-browser--loading');

                var data = {
                    action: 'cpsc_load_posts',
                    args: args,
                };
                $.post(cpsc.ajax_url, data, function (response) {
                    $(document.body).css({ 'cursor': 'default' });
                    $browser.removeClass('posts-browser--loading');
    
                    if (response) {
                        $postList.html(response.html);
                        $postList.find('sr-only').last().trigger('focus');
    
                        if (args['posts_per_page'] > response.found) {
                            $browser.find('[data-action="load-more"]').hide();
                        }
                        else {
                            $browser.find('[data-action="load-more"]').show();
                        }
                    }
                });
            }

            if (val) {
                $(this).parents('.dropdown').addClass('selected').children('button').find('.current').text($(this).text());
            }
            else {
                $(this).parents('.dropdown').addClass('selected').removeClass('selected');
            }
        });
	});
})(jQuery);