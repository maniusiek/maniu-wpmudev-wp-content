<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package cpschool
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();

$wrapper_class = 'main-wrapper wrapper';

$hero_style = cpschool_get_hero_style();

$wrapper_class .= $hero_style === 'big-image' ? ' wrapper--big-image-hero' : '';

?>

<div class="<?php echo esc_attr( $wrapper_class ); ?>" id="page-wrapper">
	<div <?php cpschool_class( 'content', 'container' ); ?> id="content">
		<?php if ( $hero_style === 'big-image' ) : ?>
			<div class="row">
				<div class="col-md-12">
					<div class="page-wrapper-inner">
		<?php endif; ?>
			<div class="row">
				<!-- Do the left sidebar check -->
				<?php get_template_part( 'template-parts/global-templates/left-sidebar-check' ); ?>

				<main class="site-main" id="main">
					<?php
					while ( have_posts() ) :
						the_post();
						?>
						<?php get_template_part( 'template-parts/loop-templates/content-singular', 'page' ); ?>

						<?php
						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
						?>
					<?php endwhile; // end of the loop. ?>
				</main><!-- #main -->

				<!-- Do the right sidebar check -->
				<?php get_template_part( 'template-parts/global-templates/right-sidebar-check' ); ?>
			</div><!-- .row -->
		<?php if ( $hero_style === 'big-image' ) : ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div><!-- #content -->
</div><!-- #page-wrapper -->

<?php
get_footer();
