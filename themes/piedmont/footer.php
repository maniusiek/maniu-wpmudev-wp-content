<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package cpschool
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;
?>

<footer class="wrapper has--bg-texture" id="wrapper-footer">
	<div id="footer-content">
		<div class="container">
			<div class="row footer__primary">
				<div class="col-12 col-md-4">
					<a href="<?php echo get_site_url(); ?>" class="footer-brand" rel="home">
						<img width="267" height="47"
						     src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-reversed.svg" class="img-fluid"
						     alt="<?php esc_html_e( 'Piedmont University', 'piedmont' ); ?>">
					</a>
				</div>
				<div class="col-12 col-md-4 footer-contact-details__wrapper">
					<?php
					if ( function_exists( 'get_field' ) ) {
						$footer_contact_details = get_field( 'contact_details', 'option' );
						if ( $footer_contact_details ) {
							$add_line = false;
							?>
							<div class="footer-widget__title"><?php esc_html_e( 'Contact', 'piedmont' ); ?></div>

							<ul class="footer-contact-details">
								<?php
								foreach ( $footer_contact_details as $info ) {
									if ( ! empty( trim( $info['info'] ) ) ) {
										echo '<li>' . $info['info'] . '</li>';
									}
								}
								?>
							</ul>
							<?php
						}
					}
					?>
				</div>

				<div class="col-12 col-md-4 footer-helpful-links__wrapper">
					<div class="footer-widget__title footer-helpful-links__header"><?php esc_html_e('Helpful Links', 'piedmont');?></div>
					<?php
					wp_nav_menu(
						array(
							'theme_location'       => 'helpful-links',
							'container'            => 'nav',
							'container_class'      => 'nav-nav-container helpful-links',
							'container_id'         => 'nav-helpful-links-main',
							'container_aria_label' => __( 'helpful Links', 'piedmont' ),
							'menu_class'           => 'nav',
							'fallback_cb'          => '',
							'menu_id'              => 'menu-helpful-links-main',
							'depth'                => 1,
							'walker'               => new CPSchool_WP_Bootstrap_Navwalker(),
						)
					);
					?>
				</div>
			</div>
			<div class="row footer__secondary">
				<div class="col-12 footer-separator__wrapper">
					<div class="footer-separator"></div>
				</div>
				<div class="col-12 col-md-4 order-md-last social-link__wrapper">
					<?php cpcs_social_links( 'footer' ); ?>
				</div>
				<div class="col-12 col-md-8 menu-footer__wrapper">
					<?php
					wp_nav_menu(
						array(
							'theme_location'       => 'footer',
							'container'            => 'nav',
							'container_class'      => 'nav-container',
							'container_id'         => 'nav-footer',
							'container_aria_label' => __( 'footer', 'piedmont' ),
							'menu_class'           => 'nav',
							'fallback_cb'          => '',
							'menu_id'              => 'menu-footer',
							'depth'                => 1,
							'walker'               => new CPSchool_WP_Bootstrap_Navwalker( true ),
						)
					);
					?>
				</div>
			</div>
		</div>
	</div>
</footer><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->


<?php
get_template_part( 'template-parts/global-templates/modal', 'slide-in-menu' );
?>

<?php wp_footer(); ?>

</body>

</html>
