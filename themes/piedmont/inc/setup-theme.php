<?php

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 */
add_action(
	'after_setup_theme',
	function() {
		// Register all menus used in theme.
		register_nav_menus(
			array(
				'desktop'          => __( 'Desktop Main Menu', 'piedmont' ),
				'desktop-extended' => __( 'Desktop Slide-In Menu', 'piedmont' ),
				'info'             => __( 'Info Menu', 'piedmont' ),
				'helpful-links'    => __( 'Helpful Links', 'piedmont' ),
				'footer'           => __( 'Footer', 'piedmont' ),
			)
		);

		// Register WP genereted <title> tag.
		add_theme_support( 'title-tag' );

		// Register image sizes
		add_theme_support( 'post-thumbnails' );
		//set_post_thumbnail_size( 295, 197, true );
		add_image_size( 'hero', 2560 );

		// Image Gallery Image size
		add_image_size( 'image_gallery',  1280 );

		// Image Slider Image size
		add_image_size( 'image_slider',  1077, 579, true );

		// Stories Slider Image size
		add_image_size( 'story_slider',  435, 318, true );

		// Featured Events Image size
		add_image_size( 'featured_events',  363, 234, true );

		// Testimonial Thumbnail
		add_image_size( 'testimonial_thumbnail',  200, 240, true );

		// Add support for wide blocks
		add_theme_support( 'align-wide' );

		// Add custom colors to blocks.
		$color_pallete = array(
			array(
				'name'  => __( 'A', 'piedmont' ),
				'slug'  => 'a',
				'color' => '#02381f',
			),
			array(
				'name'  => __( 'B', 'piedmont' ),
				'slug'  => 'b',
				'color' => '#03502c',
			),
			array(
				'name'  => __( 'C', 'piedmont' ),
				'slug'  => 'c',
				'color' => '#DEEFDB',
			),
			array(
				'name'  => __( 'D', 'piedmont' ),
				'slug'  => 'd',
				'color' => '#b5a36a',
			),
			array(
				'name'  => __( 'E', 'piedmont' ),
				'slug'  => 'e',
				'color' => '#e2cb7d',
			),
			array(
				'name'  => __( 'F', 'piedmont' ),
				'slug'  => 'f',
				'color' => '#EEEEEE',
			),
			array(
				'name'  => __( 'G', 'piedmont' ),
				'slug'  => 'g',
				'color' => '#FFFFFF',
			),
		);
		add_theme_support( 'editor-color-palette', $color_pallete );

		// Set fonts sizes based on font size customizer setting.
		add_theme_support(
			'editor-font-sizes',
			array(
				array(
					'name' => __( 'Small', 'piedmont' ),
					'size' => 16,
					'slug' => 'small',
				),
				array(
					'name' => __( 'Normal', 'piedmont' ),
					'size' => 18,
					'slug' => 'normal',
				),
				array(
					'name' => __( 'Medium', 'piedmont' ),
					'size' => 22,
					'slug' => 'medium',
				),
				array(
					'name' => __( 'Medium Large', 'piedmont' ),
					'size' => 24,
					'slug' => 'medium-large',
				),
				array(
					'name' => __( 'Large', 'piedmont' ),
					'size' => 32,
					'slug' => 'large',
				),
				array(
					'name' => __( 'Extra Large', 'piedmont' ),
					'size' => 52,
					'slug' => 'extra-large',
				),
				array(
					'name' => __( 'Huge', 'piedmont' ),
					'size' => 60,
					'slug' => 'larger',
				),
			)
		);
		add_theme_support(
			'editor-gradient-presets',
			array(
				array(
					'name'     => __( 'Dark green to white', 'piedmont' ),
					'gradient' => 'linear-gradient(180deg, #cfded7 0%, #ffffff 100%) 0% 0% no-repeat padding-box',
					'slug'     => 'green-white'
				),
				array(
					'name'     => __( 'Dark green to pearl', 'piedmont' ),
					'gradient' => 'linear-gradient(180deg, #cfded7 0%, #EAE5D9 100%) 0% 0% no-repeat padding-box',
					'slug'     =>  'green-pearl',
				),
				array(
					'name'     => __( 'Light green to pearl', 'piedmont' ),
					'gradient' => 'linear-gradient(180deg, #96C4AF 0%, #EAE5D9 100%) 0% 0% no-repeat padding-box',
					'slug'     =>  'light-green-pearl',
				),
				array(
					'name'     => __( 'White to Green', 'piedmont' ),
					'gradient' => 'linear-gradient(180deg, #F8F8F8 0%, #C8E2D6 100%) 0% 0% no-repeat padding-box',
					'slug'     =>  'white-green',
				),
			)
		);
		register_sidebar(
			array(
				'name'          => __( 'Footer Full', 'cpschool' ),
				'id'            => 'footerfull',
				'description'   => __( 'Full sized footer widget with dynamic grid', 'cpschool' ),
				'before_widget' => '<div id="%1$s" class="footer-widget widget col-12 col-md-6 col-lg order-1 order-lg-2">',
				'after_widget'  => '</div><!-- .footer-widget -->',
				'before_title'  => '<h3 class="h widget-title">',
				'after_title'   => '</h3>',
			)
		);
	}
);
