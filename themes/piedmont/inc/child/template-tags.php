<?php
// Disables all default Flex sidebars.
function cpschool_get_active_sidebars() {
	return array();
}

// Some default functions that might come in handy... or not

function cpcs_get_hero_page_id() {
	if ( is_home() ) {
		$post_id = get_option( 'page_for_posts' );
	} elseif ( is_singular() ) {
		$post_id = get_the_ID();
	} elseif ( is_post_type_archive( 'calendar_event' ) ) {
		//$post_id = cpcs_get_events_page_id();
	} else {
		$post_id = false;
	}

	return $post_id;
}

function cpcs_get_hero_thumbnail_post_id() {
	if ( is_singular() && has_post_thumbnail( get_the_ID() ) ) {
		$post_id = get_the_ID();
	} elseif ( get_post_type() == 'post' || is_search() ) {
		$post_id = get_option( 'page_for_posts' );
	} elseif ( get_post_type() == 'calendar_event' || is_post_type_archive( 'calendar_event' ) ) {
		$post_id = cpcs_get_events_page_id();
	} else {
		$post_id = false;
	}

	return $post_id;
}

function cpschool_get_hero_style() {
	$hero_style = 'regular';

	if ( is_home() || is_search() || in_array( get_post_type(), array( 'post', 'calendar_event', 'directory' ) ) ) {
		$hero_style = 'narrow';
	} else {
		$post_id = cpcs_get_hero_page_id();
		if ( $post_id ) {
			$custom_hero_style = get_post_meta( $post_id, 'cpsc_hero_style', true );
			if ( $custom_hero_style ) {
				if ( $custom_hero_style == 'disabled' ) {
					return false;
				}
				$hero_style = $custom_hero_style;
			}
		}
	}

	return $hero_style;
}

function cpschool_get_page_title() {
	$title = false;

	if ( is_search() ) {
		$title = sprintf(
			'%1$s %2$s',
			'<span class="color-accent">' . __( 'Search:', 'piedmont' ) . '</span>',
			'&ldquo;' . get_search_query() . '&rdquo;'
		);
	} elseif ( is_singular() ) {
		$title = get_the_title();
	} elseif ( is_404() ) {
		$title = __( 'Oops! That page can&rsquo;t be found.', 'piedmont' );
	} elseif ( is_archive() ) {
		$title = get_the_archive_title();
	} elseif ( is_home() ) {
		$title = get_theme_mod( 'posts_main_hero_title' );
		if ( ! $title ) {
			$blog_page_id = get_option( 'page_for_posts' );
			if ( $blog_page_id ) {
				$title = get_the_title( $blog_page_id );
			}
		}
	}

	return $title;
}

function cpcs_social_links( $modifier = '' ) {
	if ( function_exists( 'get_field' ) && have_rows( 'social_link', 'option' ) ) {

		?>
		<ul class="nav <?php echo $modifier ? ' social-icons--' . esc_attr( $modifier ) : ''; ?>">
			<?php while ( have_rows( 'social_link', 'option' ) ): the_row(); ?>
				<?php
				if ( $url = get_sub_field( 'instagram_link' ) ):
					?>
					<li class="social-icons__item">
						<a class="social-icons__link" href="<?php echo esc_url( $url ); ?>">
							<i class="social-icons__icon cps-icon cps-icon-instagram" aria-hidden="true"></i>
							<span class="sr-only"><?php _e( 'Instagram', 'piedmont' ); ?></span>
						</a>
					</li>
				<?php
				endif;
				if ( $url = get_sub_field( 'facebook_link' ) ):
					?>
					<li class="social-icons__item">
						<a class="social-icons__link" href="<?php echo esc_url( $url ); ?>">
							<i class="social-icons__icon cps-icon cps-icon-facebook-square" aria-hidden="true"></i>
							<span class="sr-only"><?php _e( 'Facebook', 'piedmont' ); ?></span>
						</a>
					</li>
				<?php
				endif;
				if ( $url = get_sub_field( 'twitter_link' ) ):
					?>

					<li class="social-icons__item">
						<a class="social-icons__link"
						   href="<?php echo esc_url( $url ); ?>">
							<i class="social-icons__icon cps-icon cps-icon-twitter" aria-hidden="true"></i>
							<span class="sr-only"><?php _e( 'Twitter', 'piedmont' ); ?></span>
						</a>
					</li>
				<?php
				endif;
				if ( $url = get_sub_field( 'youtube_link' ) ):
					?>
					<li class="social-icons__item">
						<a class="social-icons__link"
						   href="<?php echo esc_url( $url ); ?>">
							<i class="social-icons__icon cps-icon cps-icon-youtube-play" aria-hidden="true"></i>
							<span class="sr-only"><?php _e( 'YouTube', 'piedmont' ); ?></span>
						</a>
					</li>
				<?php
				endif;
				if ( $url = get_sub_field( 'linkedin_link' ) ):
					?>
					<li class="social-icons__item">
						<a class="social-icons__link"
						   href="<?php echo esc_url( $url ); ?>">
							<i class="social-icons__icon cps-icon cps-icon-linkedin-square" aria-hidden="true"></i>
							<span class="sr-only"><?php _e( 'Linkedin', 'piedmont' ); ?></span>
						</a>
					</li>
				<?php
				endif;
			endwhile; ?>
		</ul>
		<?php
		$url = get_sub_field( 'all_social_media' );
		?>
		<div class="social-icons__text social-icons__item--last">
			<a class="social-icons__link"
			   href="<?php echo esc_url( $url ); ?>">
				<span class="right-arrow"><?php _e( 'all social media', 'piedmont' ); ?></span>
			</a>
		</div>
		<?php
	}
}
