<?php

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page(
		array(
			'page_title' => __( 'Theme Options' ),
		)
	);
}

if ( function_exists( 'acf_register_block_type' ) ) {
	add_action(
		'acf/init',
		function() {
			acf_register_block_type(
				array(
					'name'            => 'cpsc_social',
					'title'           => __( 'Social Icons', 'piedmont' ),
					'render_template' => 'template-parts/blocks/social-icons.php',
					'icon'            => 'share',
					'keywords'        => array( 'social' ),
				)
			);

			acf_register_block_type(
				array(
					'name'            => 'cpsc_hero_slider',
					'title'           => __( 'Hero Slider', 'piedmont' ),
					'render_template' => 'template-parts/blocks/hero-slider.php',
					'icon'            => 'slides',
					'keywords'        => array( 'slider', 'hero', 'home' ),
				)
			);

			acf_register_block_type(
				array(
					'name'            => 'cpsc_stories_slider',
					'title'           => __( 'Stories Slider', 'piedmont' ),
					'render_template' => 'template-parts/blocks/stories-slider.php',
					'icon'            => 'slides',
					'keywords'        => array( 'slider', 'stories', 'home' ),
				)
			);

			acf_register_block_type(
				array(
					'name'            => 'cpsc_important_dates',
					'title'           => __( 'Important Dates', 'piedmont' ),
					'render_template' => 'template-parts/blocks/important-dates.php',
					'icon'            => 'calendar',
					'keywords'        => array( 'home', 'event', 'important date' ),
				)
			);

			acf_register_block_type(
				array(
					'name'            => 'cpsc_image_gallery',
					'title'           => __( 'Image Gallery', 'piedmont' ),
					'render_template' => 'template-parts/blocks/image-gallery.php',
					'icon'            => 'slides',
					'keywords'        => array( 'gallery', 'image', 'image gallery' ),
				)
			);

			acf_register_block_type(
				array(
					'name'            => 'cpsc_accordion',
					'title'           => __( 'Accordion', 'piedmont' ),
					'render_template' => 'template-parts/blocks/accordion.php',
					'icon'            => 'archive',
					'keywords'        => array( 'accordion', 'toggle' ),
				)
			);

			acf_register_block_type(
				array(
					'name'            => 'cpsc_testimonial',
					'title'           => __( 'Testimonial', 'piedmont' ),
					'render_template' => 'template-parts/blocks/testimonial.php',
					'icon'            => 'editor-quote',
					'keywords'        => array( 'testimonial', 'quote' ),
				)
			);

			acf_register_block_type(
				array(
					'name'            => 'cpsc_image_slider',
					'title'           => __( 'Image Slider', 'piedmont' ),
					'render_template' => 'template-parts/blocks/image-slider.php',
					'icon'            => 'slides',
					'keywords'        => array( 'image', 'slider' ),
				)
			);
			acf_register_block_type(
				array(
					'name'            => 'cpsc_featured_news',
					'title'           => __( 'Featured News', 'piedmont' ),
					'render_template' => 'template-parts/blocks/featured-news.php',
					'icon'            => 'align-pull-left',
					'keywords'        => array( 'news', 'blog' ),
				)
			);
			acf_register_block_type(
				array(
					'name'            => 'cpsc_stats_slider',
					'title'           => __( 'Stats Slider', 'piedmont' ),
					'render_template' => 'template-parts/blocks/stats-slider.php',
					'icon'            => 'slides',
					'keywords'        => array( 'stats', 'statistics', 'slider' ),
				)
			);
			acf_register_block_type(
				array(
					'name'            => 'cpsc_articles_browser',
					'title'           => __( 'Articles Browser', 'piedmont' ),
					'render_template' => 'template-parts/blocks/articles-browser.php',
					'keywords'        => array( 'articles', 'browser' ),
				)
			);
			acf_register_block_type(
				array(
					'name'            => 'cpsc_featured_events',
					'title'           => __( 'Featured Events', 'piedmont' ),
					'render_template' => 'template-parts/blocks/featured-events.php',
					'keywords'        => array( 'featured', 'events' ),
				)
			);
			acf_register_block_type(
				array(
					'name'            => 'cpsc_know_more',
					'title'           => __( 'Know more links', 'piedmont' ),
					'render_template' => 'template-parts/blocks/know-more.php',
					'icon'            => 'admin-links',
					'keywords'        => array( 'piedmont', 'link' ),
				)
			);
			acf_register_block_type(
				array(
					'name'            => 'cpsc_events_browser',
					'title'           => __( 'Events Browser' ),
					'render_template' => 'template-parts/blocks/events-browser.php',
					'icon'            => 'calendar-alt',
					'keywords'        => array( 'news' ),
				)
			);
		}
	);
}

add_filter(
	'acf/settings/save_json',
	function( $path ) {
		$path = get_stylesheet_directory() . '/inc/child/acf/';

		return $path;
	}
);

add_filter(
	'acf/settings/load_json',
	function( $paths ) {
		unset( $paths[0] );
		$paths[] = get_stylesheet_directory() . '/inc/child/acf/';

		return $paths;
	}
);
