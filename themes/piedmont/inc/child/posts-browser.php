<?php
add_action( 'wp_ajax_cpsc_load_posts', 'cpsc_load_posts' );
add_action( 'wp_ajax_nopriv_cpsc_load_posts', 'cpsc_load_posts' );
function cpsc_load_posts() {    
	$args         = $_REQUEST['args'];
	$allowed_args = array( 'post_type', 'posts_per_page', 'category__not_in', 'cat', 'paged', 'tax', 'tax__not_in', 'from', 'suppress_filters' );
	foreach ( $args as $arg_key => $arg_value ) {
		if ( ! in_array( $arg_key, $allowed_args ) ) {
            unset( $args[ $arg_key ] );
        }
    }
    if( isset( $args['posts_per_page'] ) && $args['posts_per_page'] > 20 ) {
        $args['posts_per_page'] = 20;
    }
    if( isset( $args['tax'] ) || isset( $args['tax__not_in'] ) ) {
        $args['tax_query'] = array( 'relation' => 'AND' );
    }
    if( isset( $args['tax'] ) ) {
        foreach( $args['tax'] as $tax_name => $tax_value ) {
            if( $tax_value ) {
                if( !is_array( $tax_value ) ) {
                    $tax_value = array( $tax_value );
                }
                $args['tax_query'][] = array(
                    'taxonomy' => $tax_name,
                    'field'    => 'term_id',
                    'terms'    => $tax_value,
                );
            }
        }
        unset( $args['tax'] );
    }
    if( isset( $args['tax__not_in'] ) ) {
        foreach( $args['tax__not_in'] as $tax_name => $tax_value ) {
            if( $tax_value ) {
                $args['tax_query'][] = array(
                    'taxonomy' => $tax_name,
                    'field'    => 'term_id',
                    'terms'    => $tax_value,
                    'operator' => 'NOT IN',
                );
            }
        }
        unset( $args['tax__not_in'] );
    }

	if ( ! isset( $args['post_type'] ) || ! is_post_type_viewable( $args['post_type'] ) ) {
		$args['post_type'] = 'post';
    }

    $args['suppress_filters'] = false;

    //var_export($args);
	$posts = get_posts( $args );

	ob_start();

	echo '<li class="sr-only" tabindex="-1">' . sprintf( __( '%d items loaded', 'piedmont' ), count( $posts ) ) . '</li>';
	if ( $posts ) {
		foreach ( $posts as $post_object ) {
			$template = get_stylesheet_directory() . '/inc/child/posts-browser/' . $args['post_type'] . '.php';
			if ( file_exists( $template ) ) {
				include( $template );
			}
		}
	}

	$response = array(
		'html'  => ob_get_clean(),
		'found' => count( $posts ),
	);

	wp_send_json( $response );
}
