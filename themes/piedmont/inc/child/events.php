<?php
add_filter(
	'posts_fields',
	function( $fields, $query ) {
		if ( ( ! is_admin() || wp_doing_ajax() ) && ! $query->is_main_query() && $query->get( 'post_type' ) == 'calendar_event' ) {
			$fields .= ', cal.from_date, cal.until_date, cal.from_time, cal.until_time';
		}

		return $fields;
	},
	10,
	2
);

add_filter(
	'posts_clauses',
	function( $clauses, $query ) {
		global $wpdb;

		if ( ( ! is_admin() || wp_doing_ajax() ) && ! $query->is_main_query() && $query->get( 'post_type' ) == 'calendar_event' ) {
			$clauses['join']   .= " RIGHT JOIN $wpdb->calendarp_calendar cal ON $wpdb->posts.ID = cal.event_id ";
			$clauses['groupby'] = ' cal.event_id';

			$from         = explode( '-', $query->get( 'from' ) );
			$from_is_date = is_array( $from ) && count( $from ) === 3 && checkdate( $from[1], $from[2], $from[0] );
			$to           = explode( '-', $query->get( 'to' ) );
			$to_is_date   = is_array( $to ) && count( $to ) === 3 && checkdate( $to[1], $to[2], $to[0] );

			$where_not = array();
			if ( $from_is_date ) {
				$where_not[] = $wpdb->prepare( 'cal.until_date < %s', implode( '-', $from ) );
			}

			if ( $to_is_date ) {
				$where_not[] = $wpdb->prepare( 'cal.from_date > %s', implode( '-', $to ) );
			}

			if ( ! $from_is_date && ! $to_is_date ) {
				$date        = date( 'Y-m-d', current_time( 'timestamp' ) );
				$where_not[] = $wpdb->prepare( 'cal.until_date < %s', $date );
			}

			$where_not         = implode( ' OR ', $where_not );
			$clauses['where'] .= " AND NOT ( $where_not )";

			if ( $query->get( 'orderby' ) === 'date' && $from_is_date ) {
				$clauses['orderby'] = 'cal.from_date ASC';
			}
		}

		return $clauses;
	},
	10,
	2
);

add_filter(
	'breadcrumb_trail_items',
	function( $items, $args ) {
		if ( get_post_type() == 'calendar_event' ) {
			   array_splice( $items, 1, -1, array( '<a href="' . esc_url( get_permalink( get_field( 'events_page', 'option' ) ) ) . '">Events</a>' ) );
		}

		return $items;
	},
	10,
	2
);

function cpcs_get_event_date_data( $event_id ) {
	$result = array(
		'dates' => 0,
	);
	$event  = calendarp_get_event( $event_id );
	if ( ! $event ) {
		return $result;
	}

	$calendar = $event->get_dates_list();
	if ( empty( $calendar ) ) {
		return $result;
	}

	$result['dates'] = count( $calendar );

	if ( 1 === count( $calendar ) ) {
		$timestamp = strtotime( $calendar[0]['from_date'] );

		$result['timestamp'] = $timestamp;
		if ( ! empty( $timestamp ) ) {
			$result['month'] = date( 'M', $timestamp );
			$result['day']   = date( 'd', $timestamp );

			if ( ! $event->is_all_day_event() ) {
				$from           = calendarp_get_formatted_time( $calendar[0]['from_time'] );
				$to             = calendarp_get_formatted_time( $calendar[0]['until_time'] );
				$result['time'] = $from === $to
					? $from
					: sprintf( '%s - %s', $from, $to );
			}
		}

		return $result;
	}

	$from = strtotime( $calendar[0]['from_date'] );
	$to   = strtotime( end( $calendar )['from_date'] );

	$result['from'] = $from;
	$result['to']   = $to;
	if ( ! empty( $from ) && ! empty( $to ) ) {
		$result['datespan'] = sprintf(
			__( '%1$s to %2$s', 'avuni' ),
			( date( 'M', $from ) . ' ' . date( 'd', $from ) ),
			( date( 'M', $to ) . ' ' . date( 'd', $to ) )
		);
	}

	if ( ! $event->is_all_day_event() ) {
		$times = array_unique( wp_list_pluck( $calendar, 'from_time' ) );
		if ( 1 === count( $times ) ) {
			$result['time'] = calendarp_get_formatted_time( $times[0] );
		}
	}

	return $result;
}

function cpcs_get_events_page_id() {
	if ( function_exists( 'calendarp_get_setting' ) ) {
		return calendarp_get_setting( 'events_page_id' );
	}

	return false;
}
