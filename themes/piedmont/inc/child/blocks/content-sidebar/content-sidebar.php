<?php
defined( 'ABSPATH' ) || exit;

function cpsc_block_sidebar_content() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	wp_register_script(
		'cpsc-block-content-sidebar',
		get_stylesheet_directory_uri() . '/inc/child/blocks/content-sidebar/block.js',
		array( 'wp-blocks', 'wp-element', 'wp-block-editor' ),
		filemtime( get_stylesheet_directory() . '/inc/child/blocks/content-sidebar/block.js' ),
		true
	);

	register_block_type(
		'cps/content-sidebar',
		array(
			'editor_script' => 'cpsc-block-content-sidebar',
		)
	);

}
add_action( 'init', 'cpsc_block_sidebar_content' );
