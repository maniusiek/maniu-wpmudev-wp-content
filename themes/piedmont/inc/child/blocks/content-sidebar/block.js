(function (blocks, element, blockEditor) {
	var el = element.createElement;
	var InnerBlocks = blockEditor.InnerBlocks;

	blocks.registerBlockType('cps/content-sidebar', {
		title: 'Content With Sidebar',
		category: 'layout',
		supports: {
			html: false
		},
		edit: function (props) {
			var className = 'content-sidebar';
			if( props.className ) {
				className += ' ' +  props.className;
			}

			return el(
				'div',
				{ className: props.className },
				el(InnerBlocks, {
					template: [['cps/content-sidebar-sidebar'], ['cps/content-sidebar-content']],
					templateLock: 'all',
					allowedBlocks: ['cps/content-sidebar-sidebar', 'cps/content-sidebar-content'],
				}),
			);
		},
		save: function (props) {
			var className = 'content-sidebar';
			if( props.className ) {
				className += ' ' +  props.className;
			}

			return el(
				'div',
				{ className: props.className },
				el(InnerBlocks.Content)
			);
		},
	});

	blocks.registerBlockType('cps/content-sidebar-sidebar', {
		title: 'Sidebar',
		parent: ['cps/content-sidebar'],
		supports: {
			html: false
		},
		edit: function (props) {
			var className = 'content-sidebar__sidebar';
			if( props.className ) {
				className += ' ' +  props.className;
			}

			return el(
				'div',
				{ className: props.className },
				el(InnerBlocks, { 
					renderAppender: InnerBlocks.ButtonBlockAppender,
					templateLock: false,
				}),
			);
		},
		save: function (props) {
			var className = 'content-sidebar__sidebar';
			if( props.className ) {
				className += ' ' +  props.className;
			}

			return el(
				'div',
				{ className: props.className },
				el(InnerBlocks.Content)
			);
		},
	});

	blocks.registerBlockType('cps/content-sidebar-content', {
		title: 'Content',
		parent: ['cps/content-sidebar'],
		supports: {
			html: false
		},
		edit: function (props) {
			var className = 'content-sidebar__content';
			if( props.className ) {
				className += ' ' +  props.className;
			}
			return el(
				'div',
				{ className: className },
				el(InnerBlocks, { 
					renderAppender: InnerBlocks.ButtonBlockAppender,
					templateLock: false,
				}),
			);
		},
		save: function (props) {
			var className = 'content-sidebar__content';
			if( props.className ) {
				className += ' ' +  props.className;
			}
			return el(
				'div',
				{ className: className },
				el(InnerBlocks.Content)
			);
		},
	});
})(window.wp.blocks, window.wp.element, window.wp.blockEditor);
