<li class="post-list__post">
    <?php if ( has_post_thumbnail( $post_object ) ) : ?>
        <div class="post-list__post-thumb">
            <?php echo get_the_post_thumbnail( $post_object, 'medium' ); ?>
        </div>
    <?php endif; ?>
    <div class="post-list__post-content">
        <a href="<?php echo esc_url( get_permalink( $post_object ) ); ?>" class="post-list__title"><?php echo get_the_title( $post_object ); ?></a>
        <p class="post-list__content"><?php echo wp_trim_words( get_the_excerpt( $post_object ), 15, '' ); ?></p>
        <a href="<?php echo esc_url( get_permalink( $post_object ) ); ?>" class="post-list__more-link right-arrow" aria-hidden="true"><?php _e( 'Keep Reading' ); ?></a>
    </div>
</li>