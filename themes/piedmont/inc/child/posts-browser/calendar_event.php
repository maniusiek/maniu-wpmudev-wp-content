<?php
$date_info = cpcs_get_event_date_data( $post_object->ID );
?>
<li class="post-list__post">
	<div class="post-list__date">
		<div class="post-list__date-content">
			<?php echo $date_info['month']; ?>
			<?php echo $date_info['day']; ?>
		</div>
	</div>
	<div class="post-list__details">
		<a href="<?php echo esc_url( get_permalink( $post_object->ID ) ); ?>" class="post-list__title">
			<?php echo get_the_title( $post_object->ID ); ?>
		</a>
	</div>
	<div class="post-list__extra-details">
		<a class="post-list__btn" href="<?php echo esc_url( get_permalink( $post_object->ID ) ); ?>" aria-hidden="true">
			<?php _e( 'Show Details', 'piedmont' ); ?>
		</a>
	</div>
</li>
